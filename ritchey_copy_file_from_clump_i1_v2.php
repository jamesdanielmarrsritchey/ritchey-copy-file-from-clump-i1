<?php
#Name:Ritchey Copy File From Clump i1 v2
#Description:Copy a file from a clump (clump must be using Ritchey Clump v2 format). Returns location of file success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is the path to a file a clump pointer file for the file being copied. 'destination' (required) is the path to write the copied file to. 'clump_location' (required) is the path to a clump. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,destination:file:required,clump_location:directory:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_copy_file_from_clump_i1_v2') === FALSE){
function ritchey_copy_file_from_clump_i1_v2($source, $destination, $clump_location, $display_errors = NULL){
	$errors = array();
	if (@is_file($source) === TRUE){
		#Do nothing
	} else {
		$errors[] = "source";
	}
	if (@is_file($destination) === FALSE){
		#Do nothing
	} else {
		$errors[] = "destination";
	}
	if (@is_file($clump_location) === TRUE){
		#Do nothing
	} else {
		$errors[] = "clump_location";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Read source file 1 line at a time, and process segment ids
		$source_handle = fopen($source, "r");
		while(feof($source_handle) === FALSE) {
			###Read block id
			$block_id = fgets($source_handle);
			$block_id = @rtrim($block_id);
			###Get from clump
			$block_base64 = FALSE;
			$clump_handle = fopen($clump_location, "r");
			$check1 = FALSE;
			$block_line_number = 0;
			if (strlen($block_id) < 1){
				$check1 = TRUE;
				$block_base64 = NULL;
			}
			while($check1 === FALSE) {
				###Read line
				$line = fgets($clump_handle);
				$block_line_number++;
				if (feof($clump_handle) !== FALSE){
					$check1 = TRUE;
				}
				if ($check1 === FALSE){
					if ($block_id == $block_line_number){
						$block_base64 = @rtrim($line);
						$check1 = TRUE;
					}
				}
			}
			fclose($clump_handle);
			if ($block_base64 === FALSE){
				$errors[] = "block_base64 (block not found in clump)";
				goto result;
			}
			###Base64 decode  block
			$block = @base64_decode($block_base64);
			###Add to file
			@file_put_contents($destination, $block, FILE_APPEND | LOCK_EX);
		}
		fclose($source_handle);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_copy_file_from_clump_i1_v2_format_error') === FALSE){
				function ritchey_copy_file_from_clump_i1_v2_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_copy_file_from_clump_i1_v2_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $destination;
	} else {
		return FALSE;
	}
}
}
?>