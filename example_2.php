<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/ritchey_copy_file_from_clump_i1_v2.php';
$source_directory = "{$location}/temporary/example_pointers";
function ritchey_list_files_i1_v1($directory){
	$directories = array("$directory");
	$list = array();
	do {
		$handle = @opendir("$directories[0]");
		if ($handle !== FALSE){
			$entry = TRUE;
			do {
				$entry = readdir($handle);
				if (@is_file("{$directories[0]}/{$entry}") === TRUE){
					@array_push($list, "{$directories[0]}/{$entry}");
				} else if (is_dir("{$directories[0]}/{$entry}") === TRUE AND "$entry" !== "." AND "$entry" !== ".." AND "$entry" !== ""){
					@array_push($directories, "{$directories[0]}/{$entry}");
				}
			} while ($entry !== FALSE);
		}
		@closedir();
		unset($directories[0]);
		$directories = @array_values($directories);
	} while (@"$directories[0]" == TRUE);
	if (@"$list[0]" == TRUE){
		$output = $list;
	} else {
		$output = FALSE;
	}
	return $output;
}
$files = ritchey_list_files_i1_v1($source_directory);
foreach ($files as $file){
	$source = $file;
	$file_name = @basename($file, '.pointer');
	$destination = "{$location}/temporary/example_files/{$file_name}";
	$clump_location = "{$location}/temporary/example_clump/clump.txt";
	$display_errors = TRUE;
	$return = ritchey_copy_file_from_clump_i1_v2($source, $destination, $clump_location, $display_errors);
	if ($return == TRUE){
		print_r($return);
		echo "\n";
	} else {
		echo "FALSE";
	}
}
?>